// npm init
/*
	- triggering this command will prompt the user for different settings that will define the application
	- using this command will make a "package.json" in our repo
	- package.json tracks the version of our application, depending on the settings we have set. also, we can see the dependencies that we have installed inside of this file
*/

/*
	npm install express
		-after triggering this command, express will now be listed as a "dependency". this can be seen inside the "package.json" file under the "dependencies" property

		- installing any dependency using npm will result into creating "node_modules" folder and package-lock.json
			"node_modules" directory should be left on the local repository because some of the hosting sites will fail to load the repository once it found the "node_modules" inside the repo. another reasone where node_modules is left on the local repository is it takes too much time to commit
			"node_modules" is also where the dependencies needed files are stored.

		".gitignore" files, as the name suggests will tell the git what files are to be spared/"ignored" in terms of commiting and pushing.

		"npm install" - this command is used when there are available dependencies inside our "package.json" but are not yet installed inside the repo/project - this is useful whentrying to clone a repo from a git repository to our local repository
*/



// we need now the express module since in this dependency, it has already built in codes that will allow the dev to create a server in a breeze/easier
const express = require("express");

// express() - allows us to create our server using express
// simply put, the app variable is our server
const app = express();

// setting up port variable
const port = 3000;

// app.use lets the server to handle data from requests
// 'app.use(express.json());' - allows the app to read and handle json data types
// methods used from express middlewares
	// middleware - software that provide common services and capabilities for the application
app.use(express.json());
// "app.use(express.urlencoded({extended: true}))" allows the server to read data from forms
// by default, information received from the url can only be received as a string or an array
// but using extended:true for the argument allows us to receive information from other data types such as objects which we will use throughout our application
app.use(express.urlencoded({extended: true}));


// SECTION-ROUTES
// GET method
/*
	Express has methods corresponding to each HTTP methods
	the route below expects to receive a GET request at the base URI "/"
*/
app.get("/", (req, res) => {
	res.send("Hello World");
});

/*
	create a "/hello" uri which will receive a get request and send a response of "Hello from /hello endpoint"
	send the output in the chat

	app.get(["/hello","/test"], (req, res) => {} - should we need same content/response/message but using different URIs - Sir Aaron
*/
app.get("/hello", (req, res) => {
	res.send("Hello from /hello endpoint");
});

// POST method
// This route expects to receive a POST request at the URI "/hello"
app.post("/hello", (req, res) =>{
	// req.body contains the contents/data of the request body
	// the properties defined in Postman request will be acessible here as properties with the same name
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

let users = [];

app.post("/signup", (req, res) => {
	console.log(req.body);
	/*
		create an if-else statement stating that:
			- if the firstName and lastName are not null, 
				the object will be pushed into the users array;
				the server will send a message confirming that the user has signed up successfully;

			- else, send a message stating that firstName and lastName should be filled with information
	*/
	if (req.body.firstName !== "" && req.body.lastName !== "") {
		users.push(req.body);
		res.send( `User ${req.body.firstName} ${req.body.lastName} has signed up successfully` );
		console.log(users);
	}
	else{
		res.send("Please input BOTH firstName and lastName");
	};
});

// PUT method
// this route expects to receive a PUT request at the URI "/change-lastName"
app.put("/change-lastName", (req, res) => {
	// initialized a variable to be used in the selection control structure codes
	let message;

	// loop through the elements of the "users" array
	for (let i = 0; i < users.length; i++){
		// checks if the user is existing in the array
		if(req.body.firstName == users[i].firstName){
			// will change the value of the lastName into a new lastName received from the request object
			users[i].lastName = req.body.lastName;

			message = `User ${req.body.firstName} has successfully changed the lastName into ${req.body.lastName}`;
			console.log(users);
			// will terminate the loop if there is a match
			break;
		// if no user is found...
		}else{
			message = "User does not exist"
		}
	}
	res.send(message);
})

// SECTION- ACTIVITY


app.get("/home", (req, res) => {
    res.send("Welcome to the home page")
})

// This route expects to receive a DELETE request at the URI "/delete-user"
// This will remove the user from the array for deletion
app.delete("/delete-user", (req, res) => {

    // Creates a variable to store the message to be sent back to the client/Postman 
    let message;

    // Creates a condition if there are users found in the array
    if (users.length != 0){

        // Creates a for loop that will loop through the elements of the "users" array
        for(let i = 0; i < users.length; i++){

            // If the username provided in the client/Postman and the username of the current object in the loop is the same
            if (req.body.firstName == users[i].firstName) {

                // The splice method manipulates the array and removes the user object from the "users" array based on it's index
                // users[i] is used here to indicate the start of the index number in the array for the element to be removed
                // The number 1 defines the number of elements to be removed from the array
                users.splice(users[i], 1);

                // Changes the message to be sent back by the response
                message = `User ${req.body.username} has been deleted.`;

                break;

            } 

        }

    // If no user was found
    } else {

        // Changes the message to be sent back by the response
        message = "User does not exist.";

    }
    
    // Sends a response back to the client/Postman once the user has been deleted or if a user is not found
    res.send(message);

})


app.get("/users", (req, res) => {
    res.send(users);
})



// Tells our server to listen to the port
// if the port is accessed, we can run the server,
// returns a message to confirm that the server is running
app.listen(port, () => console.log(`Server running at port: ${port}`));